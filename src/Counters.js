import React, { Component, Fragment } from "react";
import "./App.css";
import Counter from "./Counter";
class Counters extends Component {
  state = {
    counters: [{ id: 0, value: 0 }],
  };
  onDelete = (counterId) => {
    const counters = this.state.counters.filter((counter) => {
      return counter.id !== counterId;
    });
    this.setState({
      counters: counters,
    });
  };
  reset = () => {
    const counters = this.state.counters.map((counter) => {
      counter.value = 0;
      return counter;
    });
    this.setState({
      counters: counters,
    });
  };
  incr = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({
      counters,
    });
  };
  dcr = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value--;
    this.setState({
      counters,
    });
  };
  addCounter = () => {
    const counters = [...this.state.counters];
    counters.push({ id: counters.length + 1, value: 0 });
    this.setState({ counters: counters });
  };
  render() {
    const { counters } = this.state;
    return (
      <Fragment>
        <div className="heading">
          <p>Number of Counter : {counters.length}</p>
        </div>
        {counters.map((counter) => {
          return (
            <Counter
              delete={this.onDelete}
              key={counter.id}
              counter={counter}
              incr={this.incr}
              dcr={this.dcr}
            />
          );
        })}
        <div className="action">
          <button onClick={this.addCounter} className="add-counter">
            Add Counter
          </button>
          <button onClick={this.reset} className="reset">
            Reset
          </button>
        </div>
      </Fragment>
    );
  }
}
export default Counters;
