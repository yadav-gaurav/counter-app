import React, { Component, Fragment } from "react";
import "./App.css";
import Counters from "./Counters";
class App extends Component {
  render() {
    return (
      <Fragment>
        <Counters />
      </Fragment>
    );
  }
}
export default App;
