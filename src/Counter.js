import React, { Component, Fragment } from "react";
import "./App.css";

class Counter extends Component {
  render() {
    const { value } = this.props.counter;
    return (
      <Fragment>
        <div className="counter">
          <span className="count">{value}</span>
          <button
            onClick={() => this.props.incr(this.props.counter)}
            className="incr"
          >
            Increment
          </button>
          <button
            onClick={() => this.props.dcr(this.props.counter)}
            className="dcr"
          >
            Decrment
          </button>
          <button
            onClick={() => this.props.delete(this.props.counter.id)}
            className="delete"
          >
            Remove
          </button>
        </div>
      </Fragment>
    );
  }
}
export default Counter;
